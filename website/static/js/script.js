$("body").addClass(localStorage.getItem("theme"));

$('#dark_theme').hover(
  function() {
    $('body').addClass('dark')
  },
  function() {
    $('body').removeClass('dark')
  }
);

$('#light_theme').hover(
  function() {
    $('body').addClass('light')
  },
  function() {
    $('body').removeClass('light')
  }
);

$(document).ready(function() {
    $("#dark_theme").click(function() {
        localStorage.setItem("theme", "dark-toggled");
        $("body").removeClass("light-toggled");
        $("body").toggleClass("dark-toggled");
    });
});

$(document).ready(function() {
    $("#light_theme").click(function() {
        localStorage.setItem("theme", "light-toggled");
        $("body").removeClass("dark-toggled");
        $("body").toggleClass("light-toggled");
    });
});
